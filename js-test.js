// Déclarer une variable / constante
let a = 3;
const b = 2;


// Subtilité "var"
// Appel de la fonction possible avant sa déclaration
varExample();

function varExample(){
    if(true){
        var i = 5; // var = obsolète / portée = fonction
        let x = 8; // portée = bloc if
        console.log("Log de i (var) et x (let) dans le bloc if où elles ont été déclarées :");
        console.log(i); // 5
        console.log(x); // 8
    }
    console.log("");
    console.log("Log de i en dehors du bloc if mais dans le bloc de la fonction :");
    console.log(i); // 5
    // console.log(x); = undefined
}
// console.log(i); = undefined

console.log("");
console.log("");


// Tableau d'objets
let users = [
    {
        firstName: "Jean",
        lastName: "Paul",
        age: 32,
        pets: ['toto', 'pepette', 'cookie']
    },
    {
        firstName: "Sabine",
        lastName: "Durand",
        age: 64,
        pets: ['pepette', 'cookie']
    },
    {
        firstName: "Yoann",
        lastName: "Dupont",
        age: 15,
        pets: ['chaussette', 'frisquette']
    },
]; 

console.log("Log de tous les prénoms du tableau users :");
for(let i=0; i<users.length; i++){
    console.log(users[i].firstName);
}

console.log("");

console.log("Log de tous les prénoms et âges d'adolescents du tableau users :");
for(let i=0; i<users.length; i++){
    if(users[i].age >= 12 && users[i].age <= 20){
        console.log(users[i].firstName + " " + users[i].age);
    }
}

console.log("");

console.log("Log de tous les prénoms et âges d'adolescents du tableau users (version impérative) :");
for(let user of users){
    // Booléen pour filtrer les éléments du tableau
    const isAdolescent = user.age >= 12 && user.age <= 20;
    // Condition basée sur le booléen filtrant les éléments
    if(isAdolescent){
        console.log(user.firstName + " " + user.age);
    }
}

console.log("");

console.log("Log de tous les prénoms et âges d'adolescents du tableau users (version fonctionnelle) :");
// Booléen pour filtrer les éléments du tableau
const isAdolescent = user =>  user.age >= 12 && user.age <= 20;
// Filtre le tableau à partir du booléen et le parcourt
users.filter(isAdolescent).forEach(user => console.log(user.firstName + " " + user.age));

console.log("");

console.log("Log de tous les prénoms de personnes qui ont un animal nommé \"cookie\" (version impérative) :");
for(let user of users){
    const hasCookie = user.pets.includes('cookie');
    if(hasCookie){
        console.log(user.firstName);
    }
}

console.log("");

console.log("Log de tous les prénoms de personnes qui ont un animal nommé \"cookie\" (version fonctionnelle) :");
const hasCookie = user => user.pets.includes('cookie');
users.filter(hasCookie).forEach(user => console.log(user.firstName));

console.log("");
console.log("");


// Classe
class Car {
    constructor(nbrPlaces, nbrWheelDrive, position){
        this.nbrPlaces = nbrPlaces;
        this.nbrWheelDrive = nbrWheelDrive;
        this.position = position;
    }

    move(position){
        this.position = position;
    }

    test(){
        this.test = "test";
        // Le champ "test" existe désormais
    }

    clone(){
        return new Car(this.nbrPlaces, this.nbrWheelDrive, this.position?.slice());
    }
}

// Création d'un nouvel objet Car + set des champs "move" et "test"
const car = new Car(4, 2);
car.move([5,2]);
car.test();

console.log("Log du Car \"car\" :");
console.log(car);

// console.log("Utilisation de la méthode \"move\" sur le Car \"car\" :");
// // "move" va être appelée sur "car" toutes les 1s et incrémente "position"
// const intervalId = setInterval(() => {
//     car.move([car.position[0] + 1, car.position[1] + 1]);
//     console.log(car);
// }, 1000);
// // Applique un délai de 5s avant l'exécution des instructions
// const timeoutId = setTimeout(() => {
//     clearInterval(intervalId) // Déprogramme l'exécution de la fonction
// }, 5000);

console.log("");
console.log("");


console.log("Passage par référence vs passage par valeur/copie :");
const user = { firstName: 'Rémi' }
function setFirstNameMutation(user, newFirstName) {
    user.firstName = newFirstName;
    return user;
}
function setFirstNameImmutable(user, newFirstName) {
    // Spread operator (fonctionnalité JS): copie toutes les propriétés d'un niveau d'objet ou de tableau
    return {...user, firstName: newFirstName};
}
const user2 = setFirstNameImmutable(user, 'Paul');
const user3 = setFirstNameMutation(user, 'Jules');
console.log(user); // Jules
console.log(user2); // Paul
console.log(user3); // Jules

const car2 = new Car(5, 10);
const car3 = {...car2};
const car4 = car2.clone();
console.log(car2);
console.log(car3);
// console.log(car3.move([23,25])); // car3 instancié avec le spread operator = n'a pas de méthode "move" = erreur
console.log(car4);
console.log(car4.move([26,27])); // = car4 instancié par la méthode "clone" = OK

console.log("");
console.log("");


console.log("Utilisation de l'incrémentation ++ :");
let v = 5;
let w = v++;
// ++ incrémente v mais w vaut la valeur initiale de v
console.log(v); // 6
console.log(w); // 5
let y = 5;
let z = ++y;
// ++ incrémente y et z vaut la nouvelle valeur de y
console.log(y); // 6
console.log(z); // 6

console.log("");
console.log("");


// Filter : La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments 
// du tableau d'origine qui remplissent une condition déterminée par la fonction callback.

// const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
// const result = words.filter(word => word.length > 6);
// console.log(result);
// expected output: Array ["exuberant", "destruction", "present"]

// function suffisammentGrand(element) {
//     return element >= 10;
// }
// var filtre = [12, 5, 8, 130, 44].filter(suffisammentGrand);
// filtre vaut [12, 130, 44]  

// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/filter


// Foreach : La méthode forEach() permet d'exécuter une fonction donnée sur chaque élément du tableau.

// const array1 = ['a', 'b', 'c'];
// array1.forEach(element => console.log(element));
// expected output: "a"
// expected output: "b"
// expected output: "c"

// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach


// Map : La méthode map() crée un nouveau tableau avec les résultats de l'appel 
// d'une fonction fournie sur chaque élément du tableau appelant.

// const array1 = [1, 4, 9, 16];
// pass a function to map
// const map1 = array1.map(x => x * 2);
// console.log(map1);
// expected output: Array [2, 8, 18, 32]

// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/map


// Flat :  The flat() method creates a new array with all sub-array elements concatenated into
// it recursively up to the specified depth. 

// const arr1 = [0, 1, 2, [3, 4]];
// console.log(arr1.flat());
// expected output: [0, 1, 2, 3, 4]
// const arr2 = [0, 1, 2, [[[3, 4]]]];
// console.log(arr2.flat(2));
// expected output: [0, 1, 2, [3, 4]]

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flat


// FlatMap : The flatMap() method returns a new array formed by applying a given callback function to each 
// element of the array, and then flattening the result by one level. It is identical to a map() followed by a flat() 
// of depth 1 (arr.map(...args).flat()), but slightly more efficient than calling those two methods separately.

// const arr1 = [1, 2, [3], [4, 5], 6, []];
// const flattened = arr1.flatMap(num => num);
// console.log(flattened);
// expected output: Array [1, 2, 3, 4, 5, 6]

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flatMap


// Spread : La syntaxe de décomposition permet d'étendre un itérable (par exemple une expression de tableau ou une chaîne 
// de caractères) en lieu et place de plusieurs arguments (pour les appels de fonctions) ou de plusieurs éléments (pour les littéraux 
// de tableaux) ou de paires clés-valeurs (pour les littéraux d'objets).

// function sum(x, y, z) {
//     return x + y + z;
// }
// const numbers = [1, 2, 3];
// console.log(sum(...numbers));
// expected output: 6
// console.log(sum.apply(null, numbers));
// expected output: 6
  
// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Spread_syntax


// Passage par référence vs passage par valeur/copie

// Par valeur : (type primitif)
// function doubleBankAccount(amount) {
//  amount = amount * 2
// }
// let bankAccountAmount = 777
// doubleBankAccount(bankAccountAmount)
// console.log(bankAccountAmount)
// 777

// Par référence : (type composé)
// function addSpecialNumber(numbers) {
//     numbers.push(23)
// }
// let lottoNumbers = [32, 41, 130]
// addSpecialNumber(lottoNumbers)
// console.log(lottoNumbers)
// [32, 41, 130, 23]

// https://librecours.net/module/js/js16/pres/co/passages.html?mode=html


// Type primitif / type composé

// Primitive = pas un objet et pas de méthode
// String, Number, Boolean, Null, undefined et Symbol
// Le plus bas niveau dans l'implémentation du langage
// Elles sont NON MUTABLES

// https://developer.mozilla.org/fr/docs/Glossary/Primitive

// Composé (objet) = ensemble de propriétés

// https://developer.mozilla.org/fr/docs/Web/JavaScript/Data_structures


// Fonction callback = une fonction passée dans une autre fonction en tant qu'argument
// Cette fonction est ensuite invoquée à l'intérieur de la fonction externe (routine/action)

// function salutation(name) {
//     alert('Bonjour ' + name);
// }
// function processUserInput(callback) {
//     var name = prompt('Entrez votre nom.');
//     callback(name);
// }
// processUserInput(salutation);

// https://developer.mozilla.org/fr/docs/Glossary/Callback_function


// Récursivité = fonction pouvant faire référence à elle même et s'appeler

// var x = 0;
// while (x < 10) { // "x < 10" représente la condition d'arrêt
//   // faire quelque chose
//   x++;
// }
// CONVERTI EN
// function boucle(x) {
//     if (x >= 10) // "x >= 10" représente la condition d'arrêt (équivalent à "!(x < 10)")
//       return;
//     // faire quelque chose
//     boucle(x + 1); // l'appel récursif
// }
// boucle(0);

// https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Functions#la_r%c3%a9cursivit%c3%a9


console.log("Utilisation de for each - map - flat map :");

const users1 = [
    {
        lastName: "Saisrien",
        firstName: "Jean",
        age: 15,
        phoneOperators: [
            "SFR",
            "Bouygues",
            "Orange"
        ]
    },
    {
        lastName: "Menvussa",
        firstName: "Gérard",
        age: 30,
        phoneOperators: []
    },
    {
        lastName: "Sanchez",
        firstName: "Patrick",
        age: 45,
        phoneOperators: ['Bouygues', 'Free']
    },
    {
        lastName: "Pancho",
        firstName: "Bernard",
        age: 36,
        phoneOperators: ['Bouygues', 'SFR', 'Orange']
    }
];

console.log("");

console.log("For each pour afficher tous les prénoms :");
users1.forEach(user => console.log(user.firstName));

console.log("");

console.log("Map pour afficher un tableau contenant tous les prénoms :");
console.log(users1.map(user => user.firstName));

console.log("");

// console.log(users.flatMap(user => user.firstName));
// console.log(users.filter(user => user.firstName));

// Fonctionnement de forEach
function functionForEach(array, callback) {
    for (let elem of array) {
        callback(elem);
    }
}

console.log("Fonction forEach créée pour afficher tous les prénoms :");
functionForEach(users1, user => console.log(user.firstName));

console.log("");

// Fonctionnement de map
function functionMap(array, callback){
    const result = [];
    for(let elem of array){
        result.push(callback(elem));
    }
    return result;
}

console.log("Fonction map créée pour afficher un tableau contenant tous les prénoms :");
console.log(functionMap(users1, user => user.firstName));

console.log("");

console.log("Flat map pour afficher un tableau de tous les opérateurs :");
console.log(users1.flatMap(user => user.phoneOperators));

console.log("");
console.log("");


console.log("Mettre à jour des données avec ou sans mutation :");

const vehicleA = { color: 'red', brakes: ['on', 'on', 'on', 'off'] };
const vehicleB = { color: 'blue', brakes: ['on', 'on', 'off', 'off'] };

// Fonction prenant en paramètre le véhicule à mettre à jour ainsi que la nouvelle couleur
function changeColor(vehicle, color) {
    // Spread operator permet de créer une copie à jour du véhicule sans modifier le véhicule original
    return { ...vehicle, color: color }
}
changeColor(vehicleA, 'blue');

console.log("");

console.log("Véhicule A est toujours rouge après màj avec le spread operator : ");
console.log(vehicleA);

console.log("");

console.log("Véhicule B est bleu à l'origine :");
console.log(vehicleB); // color: blue

console.log("");

console.log("Affichage direct du véhicule B avec la couleur changée en violet = c'est une copie :");
console.log(changeColor(vehicleB, "purple")); // color: purple

console.log("");

console.log("Véhicule B est toujours bleu comme à l'origine :");
console.log(vehicleB);

console.log("");

function setBrakeState(vehicle, brakeNumber, brakeState) {
    // Crée un tableau contenant tous les freins du véhicule
    const brakes = vehicle.brakes.slice();
    // Met à jour un frein
    brakes[brakeNumber] = brakeState;
    // Retourne une copie du véhicule à jour
    return {...vehicle, brakes }
}

console.log("Affichage direct du véhicule A avec le frein 3 (indice 2) OFF = c'est une copie :");
console.log(setBrakeState(vehicleA, 2, 'off').brakes); // brakes: ['on', 'on', 'off', 'off']

console.log("");

console.log("Véhicule A conserve les états de ses freins comme à l'origine :");
console.log(vehicleA.brakes); // brakes: ['on', 'on', 'on', 'off']