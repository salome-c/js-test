const { cp } = require("fs");

const Roles = {
    SuperAdmin: "SuperAdmin",
    Admin: "Admin",
    User: "User",
    Assistant: "Assistant",
}

const users = [
    { name: "Jean Paul", age: 35, roles: [Roles.User, Roles.Assistant] },
    { name: "Jean Peup", age: 26, roles: [Roles.SuperAdmin] },
    { name: "Jean Tire", age: 12, roles: [Roles.Admin] },
    { name: "Jean TirePull 1", age: 16, roles: [Roles.User] },
    { name: "Jean TirePull 2", age: 24, roles: [Roles.Admin] },
    { name: "Jean TirePlus", age: 21, roles: [Roles.Admin] },
    { name: "Jean Pull", age: 25, roles: [Roles.Assistant] },
];

console.log("Utilisateurs de plus de 20 ans et ayant le role d'admin");

// Pas bon: mauvais usage de map, utiliser un foreach à la place
users.map(user => user.age > 20 && user.roles.includes(Roles.Admin) ? console.log(user.name) : null);

// Pas mal  
users.forEach(user => {
    if(user.age > 20 && user.roles.includes(Roles.Admin)){
        console.log(user.name)
    }
});

// Map
console.log(users.map(user => user.age > 20 && user.roles.includes(Roles.Admin) ? user.name : null).filter(name => !!name));

console.log(
    users
        .filter(user => user.age > 20)
        .filter(user => user.roles.includes(Roles.Admin))
        .map(user => user.name)
    );

const adminsAbove20 = [];
for (let user of users) {
    if (user.age > 20 && user.roles.includes(Roles.Admin)) {
        adminsAbove20.push(user.name);
    }
}
console.log(adminsAbove20)

console.log("Utilisateurs de plus de 20 ans et ayant un role compris dans le tableau ci-dessous");
const searchedRoles = [Roles.Assistant, Roles.SuperAdmin];

const searchedUsers = [];
for(const user of users){
    const hasRole = user.roles.find(role => searchedRoles.find(r => r === role));
    const hasAge = user.age > 20
    if(hasAge && hasRole) {
        searchedUsers.push(user.name);
    }
}

console.log(searchedUsers)

const searchedUsers2 = users
    .filter(user => user.age > 20)
    .filter(user => user.roles.find(role => searchedRoles.find(r => r === role)))
    .map(user => user.name);

console.log(searchedUsers2)

// Recherche croisée

console.log('Log de tous les utilisateurs qui ont un critère correspondant');

const criterias = [
    { age: 26 }, 
    { name: 'Jean Paul', age: 22 }, 
    { roles: [Roles.Admin] }, 
]

function matchCriteria(user, criteria) {
    if(typeof criteria.age !== 'undefined' && user.age === criteria.age) {
        return true;
    }
    if(typeof criteria.name !== 'undefined' && user.name === criteria.name) {
        return true;
    }
    if(typeof criteria.roles !== 'undefined' && arrayEqual(user.roles, criteria.roles)) {
        return true;
    }

    return undefined;
}

const arrayEqual = function (arr1, arr2) {
	// Check if the arrays are the same length
	if (arr1.length !== arr2.length) return false;

	// Check if all items exist and are in the same order
	for (var i = 0; i < arr1.length; i++) {
		if (arr1[i] !== arr2[i]) return false;
	}

	// Otherwise, return true
	return true;
};

const searchedUsers3 = users
    .map(user => ({user, criteria: criterias.find(criteria => matchCriteria(user, criteria))}))
    .filter(({criteria}) => !!criteria)
    .map(({user, criteria}) => user.name + ' ' + JSON.stringify(criteria));

console.log(searchedUsers3);