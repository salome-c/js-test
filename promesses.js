
function getUsers() {
    let nbr = Math.round(Math.random() * 10);
    return new Promise((resolve, reject) =>  {
        const users = [];
        for(let i = 0; i < nbr; i++) {
            const id = Date.now() + i;
            const age = Math.max(10, Math.round(Math.random() * 100));
            users.push({id, firstName: 'Jean ' + id, age })
        }

        setTimeout(() => resolve(users), Math.random() * 1000);
    });
}

/*
// Log des utilisateurs
getUsers().then(result => console.log(result));

// Prénoms des utilisateurs > 25 ans
getUsers().then(users => {
    const moreThan25 = users.filter(user => user.age > 25);
    console.log("users.length");
    console.log(users.length);
    console.log("moreThan25.length");
    console.log(moreThan25.length);

    let moreThan25a = [];
    for(i = 0; i < users.length; i++) {
        if(users[i].age > 25) {
            moreThan25a.push(users[i]);
        }
    }
    console.log(moreThan25a.length);
});

function main(){
    
    const a = 1 + 1;
    console.log(a);

    lireFichier((contenu, err) => {

    });

    lireFichier().then(contenu => console.log(contenu));

    // Faut pas le faire
    // getUsers().then(result => console.log(result));

    // Log des utilisateurs
    // getUsers().then(result => console.log(result), err => console.error(err));

    // Log des utilisateurs
    // getUsers().then(result => console.log(result)).catch(err => console.error(err));

    return getUsers().then(users => users.filter(plus25));
}

function mainAsync() {

}

main().catch(err => console.error(err));
*/


function main(){
    getUsers().then(users => console.log('Users 1', users.length)).catch(err => console.error(err));
    getUsers().then(users => console.log('Users 2', users.length)).catch(err => console.error(err));
    getUsers().then(users => console.log('Users 3', users.length)).catch(err => console.error(err));

    getUsers()
     .then(usersA => getUsers().then(usersB => usersA.concat(usersB)))
     .then(usersC => getUsers().then(usersD => usersC.concat(usersD)))
     .then(users => console.log(users));
}

// main();

async function mainAsync() {
    const users = [];
    users.concat(await getUsers().then(users => {
        console.log('Appel 1');
        return users;
    }));
    users.concat(await getUsers().then(users => {
        console.log('Appel 2');
        return users;
    }));
    users.concat(await getUsers().then(users => {
        console.log('Appel 3');
        return users;
    }));
}

mainAsync();