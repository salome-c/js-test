

const func1 = function(){

}

const func2 = function(){

}

console.log(func1 === func2) // false
console.log(func1 === function(){}) // false
console.log(function(){} === function(){}) // false
console.log(func1 === func1) // true
