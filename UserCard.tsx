import React, {ChangeEvent} from "react";
import {UserDto} from './UserDto';

interface Props {
    user: UserDto;
    onSavePetNames: (userId: string, petNames: string) => void;
}

interface State {
    nbrLikes: number;
    nbrFriends: number;
    petNames: string;
}

// Faire la meme chose qu'avec le class component
export const UserCardFC = (props: Props) => {
    const {user} = props;

    let nbrLikes: number = 0;

    return (
        <div>
            <div>{user.lastName}</div>
            <div>{user.firstName}</div>
            <div>{user.age}</div>
            <div><button>Like</button> <div>Nombre de Likes: {}</div></div>
        </div>
    );
};

// Partial
export class UserCardCC extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { 
            nbrLikes: 0, 
            nbrFriends: 0,
            petNames: '',
        }
    }
    
    render() {
        const {user} = this.props;
        const {nbrLikes, petNames} = this.state;

        return (
            <div>
                <div>{user.lastName}</div>
                <div>{user.firstName}</div>
                <div>{user.age}</div>
                <div>
                    <input type='text' value={petNames} onChange={this.handleChange}></input>
                    <button onClick={this.handleSave}>Save</button>
                </div>
                <div>
                    <button onClick={() => this.setState(st => {nbrLikes: st.nbrLikes+1})}>Like</button> 
                    <div>Nombre de Likes: {nbrLikes}</div>
                </div>
            </div>
        );
    }

    private handleChange =  (ev: ChangeEvent<HTMLInputElement>) => {
        const value = ev.target.value;
        this.setState({petNames: value});
    }

    private handleSave =  () => {
        const {onSavePetNames} = this.props;
        onSavePetNames(this.props.user.id, this.state.petNames);
    }
}